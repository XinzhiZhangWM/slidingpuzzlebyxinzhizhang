package edu.wm.cs.cs301.slidingpuzzle;

import java.util.ArrayList;
import java.util.Arrays;


public class SimplePuzzleState implements PuzzleState {
	private SimplePuzzleState state1;
	private Operation op; 
	private int [][] board;
	private int dimension;
	private int pathLength;
	private int movedValue;
	
	
	public SimplePuzzleState() {
	}
	
	
	public SimplePuzzleState(SimplePuzzleState state1, Operation op, int[][] board, int pathLength, int dimension, int movedValue) {	
		this.state1 = state1; //parent is a SimplePuzzleState object
		this.op = op; //operation is one variable in Operation
		this.board = board; // current is a 2D matrix
		this.pathLength = pathLength;
		this.dimension = dimension;
		this.movedValue = movedValue; // store the value which was moved in the parent state
	}
		
	
	
	public void setToInitialState(int dimension, int numberOfEmptySlots) {
		this.dimension = dimension;
		board = new int[this.dimension][this.dimension];
		int number = 1;
		int totNumOfTiles = this.dimension * this.dimension;
		for (int i = 0; i < this.dimension; i++) {
			for (int j = 0; j < this.dimension; j++){
				if (number <= totNumOfTiles - numberOfEmptySlots) {
					board[i][j] = number;
					number++;
				}
				else {
					board[i][j] = 0;
				}
			}
		}
		this.state1 = null;
		this.op = null;
		this.pathLength = 0;
	}

	@Override
	public int getValue(int row, int column) {
		return this.board[row][column];
	}

	@Override
	public PuzzleState getParent() {
		return this.state1;
	}

	@Override
	public Operation getOperation() {
		return this.op;
	}

	@Override
	public int getPathLength() {
		return this.pathLength;
	}

	@Override
	public PuzzleState move(int row, int column, Operation op) {
		boolean possible = true;
		int new_row = 0;
		int new_column = 0;
		switch(op) {
		case MOVERIGHT:
			if((column + 1 < this.dimension) && (this.board[row][column + 1] == 0)) {
				new_row = row;
				new_column = column + 1;
				break;
			}
		case MOVELEFT:
			if((column - 1 >= 0) && (this.board[row][column - 1] == 0)) {
				new_row = row;
				new_column = column - 1;
				break;
			}
		case MOVEUP:
			if((row - 1 >= 0) && (this.board[row - 1][column] == 0)) {
				new_row = row - 1;
				new_column = column;
				break;
			}
		case MOVEDOWN:
			if((row + 1 < this.dimension) && (this.board[row + 1][column] == 0)) {
				new_row = row + 1;
				new_column = column;
				break;
			}
		default:
			possible = false;
			break;
		}
		if (possible) {
			int [][] new_board = new int[this.dimension][this.dimension];
			for (int i = 0; i < this.dimension; i++) {
				for (int j = 0; j < this.dimension; j++){
					if ((i == row) && (j == column)){
						new_board[i][j] = 0;
					}
					else if ((i == new_row) && (j == new_column)){
						new_board[i][j] = this.board[row][column];
					}
					else {
						new_board[i][j] = this.board[i][j];
					}
				}	
		}
		int pathLength = this.pathLength + 1;
		int movedValue = this.board[row][column];
		SimplePuzzleState ps = new SimplePuzzleState(this, op, new_board, pathLength, this.dimension, movedValue); //generate a new SimplePuzzleState object and pass in parameters to initialize the object and give the object newly created board
		return ps;
		}
	    return null;	
	}
	
	@Override
	public PuzzleState drag(int startRow, int startColumn, int endRow, int endColumn) {
		Operation op = null;
		int curRow = startRow;
		int curColumn = startColumn;
		SimplePuzzleState ps = new SimplePuzzleState();
		ps = this;

		while (!((curRow == endRow) && (curColumn == endColumn))) {
			int newRow = 0;
			int newColumn = 0;
			int delta_x = curRow - endRow;
			int delta_y = curColumn - endColumn;
			
			if ((delta_x < 0) && (ps.board[curRow + 1][curColumn] == 0)) {
				if (ps == this) {
					op = Operation.MOVEDOWN;
					newRow = curRow + 1;
					newColumn = curColumn;
				}
				else if (ps.op != Operation.MOVEUP) {
						op = Operation.MOVEDOWN;
						newRow = curRow + 1;
						newColumn = curColumn;
					}
				}
			if ((delta_x > 0) && (ps.board[curRow - 1][curColumn] == 0)) {
				if (ps == this) {
					op = Operation.MOVEUP;
					newRow = curRow - 1;
					newColumn = curColumn;
				}
				else if (ps.op != Operation.MOVEDOWN){
					op = Operation.MOVEUP;
					newRow = curRow - 1;
					newColumn = curColumn;
					}
				}
			
			if ((delta_y > 0) && (ps.board[curRow][curColumn - 1] == 0)) {
				if (ps == this) {
					op = Operation.MOVELEFT;
					newRow = curRow;
					newColumn = curColumn - 1;
				}
				else if (ps.op != Operation.MOVERIGHT) {
					op = Operation.MOVELEFT;
					newRow = curRow;
					newColumn = curColumn - 1;
					}
				}
			
			if ((delta_y < 0) && (ps.board[curRow][curColumn + 1] == 0)) {
				if (ps == this) {
					op = Operation.MOVERIGHT;
					newRow = curRow;
					newColumn = curColumn + 1;
				}
				else if (ps.op != Operation.MOVELEFT) {
					op = Operation.MOVERIGHT;
					newRow = curRow;
					newColumn = curColumn + 1;
				}
			}
			SimplePuzzleState new_ps = (SimplePuzzleState) ps.move(curRow, curColumn, op);
			if (new_ps == null) {
				return null;
			}
			curRow = newRow;
			curColumn = newColumn;
			ps = new_ps;
		}
		return ps;	
	}
		

	@Override
	public PuzzleState shuffleBoard(int pathLength) {
		int counter = 0;
		SimplePuzzleState ps = new SimplePuzzleState();
		ps = this;
		boolean noop = true;
		int range = 0;
		int randomInt;
		ArrayList <Operation> possibleOp = null;
		ArrayList <Integer> possibleRows = null;
		ArrayList <Integer> possibleCols = null;
		
		while (counter < pathLength) {
			ArrayList <Integer> emptyRowIndices = new ArrayList<Integer>();
			ArrayList <Integer> emptyColIndices = new ArrayList<Integer>();
			for (int i = 0; i < ps.dimension; i++) {
				for (int j = 0; j < ps.dimension; j++) {
					if (ps.board[i][j] == 0) {
						emptyRowIndices.add(i);
						emptyColIndices.add(j);
					}
				}
			}
			while (noop) { //when there are no possible operations which are valid and do not lead to loop, keep generating possible operations
				range = emptyRowIndices.size();
				randomInt = (int) (Math.random() * range);
				int emptyRowIndex = emptyRowIndices.get(randomInt);
				int emptyColIndex = emptyColIndices.get(randomInt);
			
				possibleOp = new ArrayList<Operation>();
				possibleRows = new ArrayList<Integer>();
				possibleCols = new ArrayList<Integer>();
				if ((emptyRowIndex - 1 >= 0) && (ps.board[emptyRowIndex - 1][emptyColIndex] != 0)) {
					if (! ((ps.movedValue == ps.board[emptyRowIndex - 1][emptyColIndex]) && (ps.op == Operation.MOVEUP))) {
						possibleOp.add(Operation.MOVEDOWN);
						possibleRows.add(emptyRowIndex - 1);
						possibleCols.add(emptyColIndex);
					}
				}
				if ((emptyRowIndex + 1 < ps.dimension) && (ps.board[emptyRowIndex + 1][emptyColIndex] != 0)) {
					if (! ((ps.movedValue == ps.board[emptyRowIndex + 1][emptyColIndex]) && (ps.op == Operation.MOVEDOWN))) {
						possibleOp.add(Operation.MOVEUP);
						possibleRows.add(emptyRowIndex + 1);
						possibleCols.add(emptyColIndex);
					}
				}
				if ((emptyColIndex - 1 >= 0) && (ps.board[emptyRowIndex][emptyColIndex - 1] != 0)) {
					if (! ((ps.movedValue == ps.board[emptyRowIndex][emptyColIndex - 1]) && (ps.op == Operation.MOVELEFT))) {
						possibleOp.add(Operation.MOVERIGHT);
						possibleRows.add(emptyRowIndex);
						possibleCols.add(emptyColIndex - 1);
					}
				}
				if ((emptyColIndex + 1 < ps.dimension) && (ps.board[emptyRowIndex][emptyColIndex + 1] != 0)) {
					if (! ((ps.movedValue == ps.board[emptyRowIndex][emptyColIndex + 1]) && (ps.op == Operation.MOVERIGHT))) {
						possibleOp.add(Operation.MOVELEFT);
						possibleRows.add(emptyRowIndex);
						possibleCols.add(emptyColIndex + 1);
					}
				}
				range = possibleOp.size();
				if (range != 0) {
					noop = false;
				}
			}
			
			randomInt = (int)(Math.random() * range);
			Operation operation = possibleOp.get(randomInt);
			int targetRow = possibleRows.get(randomInt);
			int targetCol = possibleCols.get(randomInt);
			
			SimplePuzzleState new_ps = new SimplePuzzleState();
			new_ps = (SimplePuzzleState) ps.move(targetRow, targetCol, operation);
			ps = new_ps;
			counter++;
			noop = true;			
		}
		return ps;
	}

	@Override
	public boolean isEmpty(int row, int column) {
		if (this.board[row][column] == 0) {
			return true;
		}
		return false;
	}

	@Override
	public PuzzleState getStateWithShortestPath() {
		return this;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimplePuzzleState other = (SimplePuzzleState) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}
}
