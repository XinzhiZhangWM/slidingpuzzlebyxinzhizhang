package edu.wm.cs.cs301.slidingpuzzle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyTest {
	@Test
	public final void testDrag() {
		PuzzleState ps1 = new SimplePuzzleState();
		PuzzleState ps2;
		PuzzleState ps3  = new SimplePuzzleState();
		
		ps1.setToInitialState(4,3);
		ps3.setToInitialState(4,3);
		// establish that ps1 and ps3 are the same by construction
		// we will use this later to check that ps1 was not changed by the drag operation
		assertEquals(ps1,ps3);
		// last 2 positions on the lower right are empty (3,2) and (3,3)
		assertFalse(ps1.isEmpty(2, 1));
		assertFalse(ps1.isEmpty(2, 2));
		assertFalse(ps1.isEmpty(2, 3));
		assertTrue(ps1.isEmpty(3, 1));
		assertTrue(ps1.isEmpty(3, 2));
		assertTrue(ps1.isEmpty(3, 3));
		// move the tile from (2,2) to (3,3)
		ps2 = ps1.drag(2, 2, 3, 3);
		// check that this worked
		assertTrue(ps2.isEmpty(2, 2));
		assertFalse(ps2.isEmpty(2, 3));
		assertTrue(ps2.isEmpty(3, 2));
		assertFalse(ps2.isEmpty(3, 3));
		// check that ps1 did not change with the drag operation
		assertEquals(ps1,ps3);
		// 2nd try, different direction
		assertFalse(ps1.isEmpty(2, 2));
		assertFalse(ps1.isEmpty(2, 3));
		assertTrue(ps1.isEmpty(3, 2));
		assertTrue(ps1.isEmpty(3, 3));
		// move tile from (2,3) to (3,2)
		ps2 = ps1.drag(2, 3, 3, 2);
		assertFalse(ps2.isEmpty(2, 2));
		assertTrue(ps2.isEmpty(2, 3));
		assertFalse(ps2.isEmpty(3, 2));
		assertTrue(ps2.isEmpty(3, 3));
		// check that ps1 did not change with the drag operation
		assertEquals(ps1,ps3);
		// move tile from (2,1) to (3,1)
		ps2 = ps1.drag(2, 1, 3, 1);
		assertFalse(ps2.isEmpty(3, 1));
		assertTrue(ps2.isEmpty(2, 1));
		assertTrue(ps2.isEmpty(3, 2));
		assertTrue(ps2.isEmpty(3, 3));
	}
	
	@Test
	public final void testShuffleBoard() {
		PuzzleState ps1 = new SimplePuzzleState();
		ps1.setToInitialState(3, 3);
		// create 2nd state and compare
		PuzzleState ps2 = new SimplePuzzleState();
		ps2.setToInitialState(3, 3);	
		assertTrue(ps1.equals(ps2));
		// use shuffle operation, length 0 should give same state
		ps2 = ps1.shuffleBoard(0);
		assertTrue(ps1.equals(ps2));
		// use shuffle operation, length 1 should give different state
		ps2 = ps1.shuffleBoard(1);
		assertFalse(ps1.equals(ps2));
		// 2nd try with length 2, see if we can do more than 1 step
		ps2 = ps1.shuffleBoard(2);
		assertFalse(ps1.equals(ps2));
	}

}
